@echo off
setlocal

:: EXAMPLE:
:: set GOPATH=c:\gopath1;c:\gopath2

:::::::: meat ::::::::

set GOROOT=%~dp0

:: get first element of GOPATH
set _tmp=%GOPATH:*;=%
call set GOPATH0=%%GOPATH:%_tmp%=%%
set GOPATH0=%GOPATH0:;=%
mkdir %GOPATH0% > nul 2>&1

:: add important folders to PATH
set PATH=%GOROOT%\bin;%GOPATH0%\bin;%PATH%

cmd /K "echo GOROOT=%GOROOT%"

endlocal

